
package com.eggprophet.cs_app.powerschool_backend.data.data_models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PublicPortalDisabledMessage {

    @SerializedName("@nil")
    @Expose
    private String nil;

    /**
     * No args constructor for use in serialization
     * 
     */
    public PublicPortalDisabledMessage() {
    }

    /**
     * 
     * @param nil
     */
    public PublicPortalDisabledMessage(String nil) {
        super();
        this.nil = nil;
    }

    public String getNil() {
        return nil;
    }

    public void setNil(String nil) {
        this.nil = nil;
    }

    public PublicPortalDisabledMessage withNil(String nil) {
        this.nil = nil;
        return this;
    }

}
