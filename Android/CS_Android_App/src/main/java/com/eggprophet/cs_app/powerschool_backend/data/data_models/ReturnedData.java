
package com.eggprophet.cs_app.powerschool_backend.data.data_models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ReturnedData {

    @SerializedName("@type")
    @Expose
    private String type;
    @SerializedName("courseRequestRulesVO")
    @Expose
    private CourseRequestRulesVO courseRequestRulesVO;
    @SerializedName("userSessionVO")
    @Expose
    private UserSessionVO userSessionVO;

    private List<Message> messageVOs;


    /**
     * No args constructor for use in serialization
     * 
     */
    public ReturnedData() {
    }

    /**
     * 
     * @param userSessionVO
     * @param courseRequestRulesVO
     * @param type
     */
    public ReturnedData(String type, CourseRequestRulesVO courseRequestRulesVO, UserSessionVO userSessionVO) {
        super();
        this.type = type;
        this.courseRequestRulesVO = courseRequestRulesVO;
        this.userSessionVO = userSessionVO;
    }

    public String getType() {
        return type;
    }

    public List<Message> getMessageVOs() { return this.messageVOs; }

    public void setType(String type) {
        this.type = type;
    }

    public ReturnedData withType(String type) {
        this.type = type;
        return this;
    }

    public CourseRequestRulesVO getCourseRequestRulesVO() {
        return courseRequestRulesVO;
    }

    public void setCourseRequestRulesVO(CourseRequestRulesVO courseRequestRulesVO) {
        this.courseRequestRulesVO = courseRequestRulesVO;
    }

    public ReturnedData withCourseRequestRulesVO(CourseRequestRulesVO courseRequestRulesVO) {
        this.courseRequestRulesVO = courseRequestRulesVO;
        return this;
    }

    public UserSessionVO getUserSessionVO() {
        return userSessionVO;
    }

    public void setUserSessionVO(UserSessionVO userSessionVO) {
        this.userSessionVO = userSessionVO;
    }

    public ReturnedData withUserSessionVO(UserSessionVO userSessionVO) {
        this.userSessionVO = userSessionVO;
        return this;
    }

    public void setMessageVOs(List<Message> messageVOs) { this.messageVOs = messageVOs;}
}
