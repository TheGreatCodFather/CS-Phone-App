
package com.eggprophet.cs_app.powerschool_backend.data.data_models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Locale {

    @SerializedName("@nil")
    @Expose
    private String nil;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Locale() {
    }

    /**
     * 
     * @param nil
     */
    public Locale(String nil) {
        super();
        this.nil = nil;
    }

    public String getNil() {
        return nil;
    }

    public void setNil(String nil) {
        this.nil = nil;
    }

    public Locale withNil(String nil) {
        this.nil = nil;
        return this;
    }

}
