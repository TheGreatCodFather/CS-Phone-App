package com.eggprophet.cs_app.login;

/**
 * Created by eggprophet on 11/02/17.
 */

public interface AsyncResponse {
    void loginFinished(Boolean validity);
    void genKeyFinished(Boolean result);
}
