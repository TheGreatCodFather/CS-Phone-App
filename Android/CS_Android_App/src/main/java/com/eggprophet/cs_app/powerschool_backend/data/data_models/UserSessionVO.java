
package com.eggprophet.cs_app.powerschool_backend.data.data_models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class UserSessionVO {

    @SerializedName("@type")
    @Expose
    private String type;
    @SerializedName("locale")
    @Expose
    private Locale locale;
    @SerializedName("serverCurrentTime")
    @Expose
    private String serverCurrentTime;
    @SerializedName("serverInfo")
    @Expose
    private ServerInfo serverInfo;
    @SerializedName("serviceTicket")
    @Expose
    private String serviceTicket;
    @SerializedName("studentIDs")
    @Expose
    private List<Long> studentIDs;
    @SerializedName("userId")
    @Expose
    private Long userId;
    @SerializedName("userType")
    @Expose
    private Long userType;

    /**
     * No args constructor for use in serialization
     * 
     */
    public UserSessionVO() {
    }

    /**
     *
     * @param serviceTicket
     * @param studentIDs
     * @param userId
     * @param locale
     * @param serverCurrentTime
     * @param type
     * @param serverInfo
     * @param userType
     */
    public UserSessionVO(String type, Locale locale, String serverCurrentTime, ServerInfo serverInfo, String serviceTicket, List<Long> studentIDs, Long userId, Long userType) {
        super();
        this.type = type;
        this.locale = locale;
        this.serverCurrentTime = serverCurrentTime;
        this.serverInfo = serverInfo;
        this.serviceTicket = serviceTicket;
        this.studentIDs = studentIDs;
        this.userId = userId;
        this.userType = userType;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public UserSessionVO withType(String type) {
        this.type = type;
        return this;
    }

    public Locale getLocale() {
        return locale;
    }

    public void setLocale(Locale locale) {
        this.locale = locale;
    }

    public UserSessionVO withLocale(Locale locale) {
        this.locale = locale;
        return this;
    }

    public String getServerCurrentTime() {
        return serverCurrentTime;
    }

    public void setServerCurrentTime(String serverCurrentTime) {
        this.serverCurrentTime = serverCurrentTime;
    }

    public UserSessionVO withServerCurrentTime(String serverCurrentTime) {
        this.serverCurrentTime = serverCurrentTime;
        return this;
    }

    public ServerInfo getServerInfo() {
        return serverInfo;
    }

    public void setServerInfo(ServerInfo serverInfo) {
        this.serverInfo = serverInfo;
    }

    public UserSessionVO withServerInfo(ServerInfo serverInfo) {
        this.serverInfo = serverInfo;
        return this;
    }

    public String getServiceTicket() {
        return serviceTicket;
    }

    public void setServiceTicket(String serviceTicket) {
        this.serviceTicket = serviceTicket;
    }

    public UserSessionVO withServiceTicket(String serviceTicket) {
        this.serviceTicket = serviceTicket;
        return this;
    }

    public List<Long> getStudentIDs() {
        return studentIDs;
    }

    public void setStudentIDs(List<Long> studentIDs) {
        this.studentIDs = studentIDs;
    }

    public UserSessionVO withStudentIDs(List<Long> studentIDs) {
        this.studentIDs = studentIDs;
        return this;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public UserSessionVO withUserId(Long userId) {
        this.userId = userId;
        return this;
    }

    public Long getUserType() {
        return userType;
    }

    public void setUserType(Long userType) {
        this.userType = userType;
    }

    public UserSessionVO withUserType(Long userType) {
        this.userType = userType;
        return this;
    }

}
