package com.eggprophet.cs_app.powerschool_backend;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import com.eggprophet.cs_app.powerschool_backend.data.data_models.Locale;
import com.eggprophet.cs_app.powerschool_backend.data.data_models.UserSessionVO;
import com.eggprophet.cs_app.powerschool_backend.data.data_models.ServerInfo;

/**
 * Created by eggprophet on 09/02/17.
 */

class UserSessionDeserializer implements JsonDeserializer<UserSessionVO> {
    @Override
    public UserSessionVO deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {

        JsonObject jObj = json.getAsJsonObject();

        JsonElement localeElement = jObj.get("locale");
        JsonElement serverInfoElement = jObj.get("serverInfo");
        JsonElement studentIDsElement = jObj.get("studentIDs");

        String type = jObj.get("@type").getAsString();
        Locale locale = new Locale();
        String serverCurrentTime = jObj.get("serverCurrentTime").getAsString();
        ServerInfo serverInfo = new ServerInfo();
        String serviceTicket = jObj.get("serviceTicket").getAsString();
        List<Long> studentIDs = new ArrayList<>();
        Long userId = jObj.get("userId").getAsLong();
        Long userType = jObj.get("userType").getAsLong();

        // Because powerschool is stupid and decides to change between a List<Long> and a Long depending on amount of students
        // linked to the account
        if (studentIDsElement.isJsonArray()) { // Multiple
            studentIDs = context.deserialize(studentIDsElement.getAsJsonArray(), new TypeToken<List<Long>>() {
            }.getType());
        } else { // Only one id
            studentIDs.add(studentIDsElement.getAsLong());
        }

        locale = context.deserialize(localeElement.getAsJsonObject(), new TypeToken<Locale>() {}.getType());
        serverInfo = context.deserialize(serverInfoElement.getAsJsonObject(), new TypeToken<ServerInfo>() {}.getType());

        return new UserSessionVO(type, locale, serverCurrentTime, serverInfo, serviceTicket, studentIDs, userId, userType);
    }
}
