package com.eggprophet.cs_app.ui_fragments;

/**
 * Created by eggprophet on 04/02/17.
 */

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.eggprophet.cs_app.R;
import com.eggprophet.cs_app.Util;

public class BugReportFragment extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.bug_report_fragment, container, false);

        setRetainInstance(true);

        final Button button = (Button) view.findViewById(R.id.bug_submit_button);
        button.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Util.openWebPage(getActivity(), "https://gitlab.com/TheGreatCodFather/CS-Phone-App/issues/new");
            }
        });

        // Inflate the layout for this fragment
        return view;
    }
}