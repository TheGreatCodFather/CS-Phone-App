
package com.eggprophet.cs_app.powerschool_backend.data.data_models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ReturnedDataWrapper {

    @SerializedName("return")
    @Expose
    private ReturnedData _return;

    private transient int responseCode;

    /**
     * No args constructor for use in serialization
     * 
     */
    public ReturnedDataWrapper() {
    }

    /**
     * 
     * @param _return
     */
    public ReturnedDataWrapper(ReturnedData _return) {
        super();
        this._return = _return;
    }

    public ReturnedData getReturn() {
        return _return;
    }

    public int getResponseCode() {
        return responseCode;
    }

    public void setReturn(ReturnedData _return) {
        this._return = _return;
    }

    public void setResponseCode(int responseCode) {
        this.responseCode = responseCode;
    }

    public ReturnedDataWrapper withReturn(ReturnedData _return) {
        this._return = _return;
        return this;
    }

}
