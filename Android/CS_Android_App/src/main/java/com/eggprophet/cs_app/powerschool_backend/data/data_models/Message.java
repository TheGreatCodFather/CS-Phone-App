package com.eggprophet.cs_app.powerschool_backend.data.data_models;

import com.google.gson.annotations.SerializedName;

public class Message {
    @SerializedName("return")
    private MessageCodes message;

    public MessageCodes getMessage() {
        return this.message;
    }

    public void setMessage(MessageCodes message) {
        this.message = message;
    }
}