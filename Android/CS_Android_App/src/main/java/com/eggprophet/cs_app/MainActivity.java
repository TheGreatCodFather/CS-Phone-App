package com.eggprophet.cs_app;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import com.eggprophet.cs_app.login.LoginActivity;
import com.eggprophet.cs_app.ui_fragments.AboutFragment;
import com.eggprophet.cs_app.ui_fragments.BugReportFragment;
import com.eggprophet.cs_app.ui_fragments.ContactsFragment;
import com.eggprophet.cs_app.ui_fragments.HomeFragment;
import com.eggprophet.cs_app.ui_fragments.StudentIDFragment;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private BroadcastReceiver broadcastReceiver = null;
    private SharedPreferences settings = null;
    private int savedSelectedNavItem = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        settings = getSharedPreferences("CPP_APP_PREFs", MODE_PRIVATE);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        if (savedInstanceState != null) {
            // Restore what ever fragment was active
            savedSelectedNavItem = savedInstanceState.getInt("savedSelectedNavItem");
            System.out.println(savedInstanceState);
            updateMenuItem(null, savedSelectedNavItem);
        } else {
            // Default to the home fragment on open
            getSupportActionBar().setTitle("Home");
            Fragment homeFrag = new HomeFragment();
            changeFragment(homeFrag);

            // Check the home fragment on open as it's the default
            navigationView.getMenu().getItem(0).setChecked(true);
        }

        IntentFilter filter = new IntentFilter();
        filter.addAction("android.net.conn.CONNECTIVITY_CHANGE");

        final Snackbar internetBar = Snackbar.make(findViewById(R.id.content_frame), "No Internet Connection", Snackbar.LENGTH_INDEFINITE);
        internetBar.setAction("Dismiss", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        }).setActionTextColor(ContextCompat.getColor(this, R.color.colorWhite));
        final Snackbar mobileDataBar = Snackbar.make(findViewById(R.id.content_frame), "Warning: you're using mobile data", Snackbar.LENGTH_INDEFINITE);
        mobileDataBar.setAction("Dismiss", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        }).setActionTextColor(ContextCompat.getColor(this, R.color.colorWhite));

        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo activeNetwork = cm.getActiveNetworkInfo();

                if (activeNetwork != null) { // connected to the internet

                    if (internetBar != null) {
                        internetBar.dismiss();
                    }
                    if (mobileDataBar != null) {
                        mobileDataBar.dismiss();
                    }

                    if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
                        View snackView = mobileDataBar.getView();
                        snackView.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.red));
                        TextView snackTextView = (TextView) snackView.findViewById(android.support.design.R.id.snackbar_text);

                        snackTextView.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);

                        mobileDataBar.show();
                    }

                } else {
                    View snackView = internetBar.getView();
                    snackView.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.red));
                    TextView snackTextView = (TextView) snackView.findViewById(android.support.design.R.id.snackbar_text);

                    snackTextView.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);

                    internetBar.show();
                }
            }
        };
        registerReceiver(broadcastReceiver, filter);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("savedSelectedNavItem", savedSelectedNavItem);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (broadcastReceiver != null) {
            unregisterReceiver(broadcastReceiver);
            broadcastReceiver = null;
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.action_account:
                DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case DialogInterface.BUTTON_POSITIVE:
                                // TODO logout of powerschool properly instead of just dropping the old session

                                // Remove credentials
                                SharedPreferences prefs = getSharedPreferences("CPP_APP_PREFs", MODE_PRIVATE);

                                SharedPreferences.Editor editor = prefs.edit();

                                // Nuke our preferences
                                editor.clear();
                                editor.apply();

                                // Go back to the login screen
                                Intent loginActivity = new Intent(getApplicationContext(), LoginActivity.class);
                                startActivity(loginActivity);
                                finish();
                                break;

                            case DialogInterface.BUTTON_NEGATIVE:
                                break;
                        }
                    }
                };
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setMessage("Are you sure? Logging out will delete your settings!").setPositiveButton("Logout", dialogClickListener)
                        .setNegativeButton("Cancel", dialogClickListener).show();
                break;
            case R.id.action_students:
                // TODO
            case R.id.action_scan:
                IntentIntegrator integrator = new IntentIntegrator(this);
                integrator.initiateScan();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        updateMenuItem(item, item.getItemId());
        return true;
    }

    private void updateMenuItem(MenuItem item, int menuItemID) {
        Fragment fragment = null;
        String title = getString(R.string.app_name);

        switch (menuItemID) {
            case 0:
            case R.id.nav_home:
                fragment = new HomeFragment();
                title = "Home";
                savedSelectedNavItem = 0;
                break;
            case 1:
            case R.id.nav_identity:
                fragment = new StudentIDFragment();
                title = "Student ID";
                savedSelectedNavItem = 1;
                break;
            case 2:
            case R.id.nav_contact:
                fragment = new ContactsFragment();
                title = "School Contacts";
                savedSelectedNavItem = 2;
                break;
            case R.id.nav_caspers:
                Util.openWebPage(this, "http://cshscaspers.weebly.com");
                break;
            case R.id.nav_fees:
                Util.openWebPage(this, "https://www.studentquickpay.com/gppsd/Default.aspx");
                break;
            case 5:
            case R.id.nav_about:
                fragment = new AboutFragment();
                title = "About";
                savedSelectedNavItem = 5;
                break;
            case 6:
            case R.id.nav_bug_report:
                fragment = new BugReportFragment();
                title = "Submit a Bug Report";
                savedSelectedNavItem = 6;
                break;

        }

        changeFragment(fragment);

        // set the toolbar title
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(title);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer != null) {
            drawer.closeDrawer(GravityCompat.START);
        }
    }

    private void changeFragment(Fragment frag) {
        if (frag != null) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.content_frame, frag);
            ft.commit();
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        IntentResult scanResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
        if (scanResult != null) {
            String result = scanResult.getContents();
            if (result != null) {
                if (result.matches("^[0-9]*$") && result.length() == 4 || result.length() == 5) {
                    settings.edit().putString("studentID", result).apply();
                } else {
                    Snackbar s = Snackbar.make(findViewById(android.R.id.content), "Barcode scan unsuccessful. Please try again.", Snackbar.LENGTH_LONG);
                    View snackView = s.getView();
                    snackView.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.red));
                    TextView snackTextView = (TextView) snackView.findViewById(android.support.design.R.id.snackbar_text);

                    snackTextView.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                    s.show();
                }
            }
        }
    }
}
