package com.eggprophet.cs_app.rss;

import android.app.Activity;
import android.graphics.Bitmap;
import android.support.v4.util.LruCache;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import com.eggprophet.cs_app.R;
import com.eggprophet.cs_app.Util;

public class RssFeedListAdapter
        extends RecyclerView.Adapter<RssFeedListAdapter.FeedModelViewHolder> {

    private Activity mActivity;
    private List<RssFeedModel> mRssFeedModels;
    private LruCache<String, Bitmap> mImageCache;

    public static class FeedModelViewHolder extends RecyclerView.ViewHolder {
        private View rssFeedView;

        public FeedModelViewHolder(View v) {
            super(v);
            rssFeedView = v;
        }
    }

    public RssFeedListAdapter(Activity activity, List<RssFeedModel> rssFeedModels, LruCache<String, Bitmap> imageCache) {
        mActivity = activity;
        mRssFeedModels = rssFeedModels;
        mImageCache = imageCache;
        setHasStableIds(true);
    }

    public Bitmap getBitmapFromMemCache(String key) {
        return mImageCache.get(key);
    }

    @Override
    public FeedModelViewHolder onCreateViewHolder(ViewGroup parent, int type) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.home_row_card, parent, false);

        FeedModelViewHolder holder = new FeedModelViewHolder(v);

        return holder;
    }

    @Override
    public void onBindViewHolder(FeedModelViewHolder holder, int position) {
        final RssFeedModel rssFeedModel = mRssFeedModels.get(position);

        ImageView imgView = (ImageView) holder.rssFeedView.findViewById(R.id.home_row_card_img);
        imgView.setImageBitmap(mImageCache.get(String.valueOf(position)));

        Animation fadeInAnimation = AnimationUtils.loadAnimation(imgView.getContext(), R.anim.fade_in);
        imgView.startAnimation(fadeInAnimation);

        Bitmap cachedBitmap = getBitmapFromMemCache(String.valueOf(position));
        imgView.setImageBitmap(cachedBitmap);

        ((TextView)holder.rssFeedView.findViewById(R.id.home_row_card_title)).setText(rssFeedModel.title);
        ((TextView)holder.rssFeedView.findViewById(R.id.home_row_card_content)).setText(rssFeedModel.description);

        holder.rssFeedView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Util.openWebPage(mActivity, rssFeedModel.link);
            }
        });
    }

    /**
     * This has the potential to result in collisions...
     * :thinkingemoji: should we implement our own map or override
     * the current hash implementation with a strong algorithm?
     */
    @Override
    public long getItemId(int position) {
        return this.mRssFeedModels.get(position).hashCode();
    }

    @Override
    public int getItemCount() {
        if (mRssFeedModels != null) {
            return mRssFeedModels.size();
        }
        return 0;
    }
}
