package com.eggprophet.cs_app.powerschool_backend;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import com.eggprophet.cs_app.powerschool_backend.data.data_models.ReturnedDataWrapper;
import com.eggprophet.cs_app.powerschool_backend.data.data_models.UserSessionVO;

/**
 * Created by eggprophet on 07/02/17.
 */

public class PowerschoolFunctions {

    private static String TAG = null;
    public static ReturnedDataWrapper returned =  null;
    public static String imageBase64 = null;

    static {
        TAG = "PublicPortalServiceClient";
    }

    public static boolean pcasLogin(String districtServerAddress, String userName, String password) {

        String xmlResponse = SoapNetworkCalls.loginToPublicPortal(districtServerAddress, userName, password);

        if (xmlResponse == null || xmlResponse.trim().length() <= 0) {
            Log.d(TAG, "xmlResponse was null in getServerInfo method!");
            return false;
        }
        Log.d(TAG, "getServerInfo XML Response: " + xmlResponse);

        try {
            // Load initial returned data -- including our UserSessionVO
            Gson gson = new GsonBuilder().registerTypeAdapter(UserSessionVO.class, new UserSessionDeserializer()).create();
            returned = gson.fromJson(xmlResponse, ReturnedDataWrapper.class);

            // Load student photo
            SoapNetworkCalls soap = new SoapNetworkCalls();
            imageBase64 = soap.getStudentPhoto("https://powerschool.gppsd.ab.ca", returned.getReturn().getUserSessionVO(), returned.getReturn().getUserSessionVO().getStudentIDs().get(0));
            JsonObject jobj = new Gson().fromJson(imageBase64, JsonObject.class);
            imageBase64 = jobj.get("return").getAsString();

            if(returned != null) {
                return true;
            }
        } catch (JsonParseException e) {
            Log.e(TAG, e.getMessage());
        }

        return false;
    }
}
