 package com.eggprophet.cs_app.login;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.util.Log;

import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;

import butterknife.ButterKnife;
import butterknife.InjectView;
import com.eggprophet.cs_app.MainActivity;
import com.eggprophet.cs_app.R;

// FIXME this could use some organization and comments
// FIXME implement token auth login method rather than simply using PCAS auth every time

public class LoginActivity extends AppCompatActivity implements AsyncResponse {

    private static final String TAG = "LoginActivity";
    private static final String DISTRICT_ADDR = "https://powerschool.gppsd.ab.ca";

    private static Context context;

    LoginCrypto crypt = null;

    @InjectView(R.id.input_username)
    EditText _usernameText;
    @InjectView(R.id.input_password)
    EditText _passwordText;
    @InjectView(R.id.remember_login)
    CheckBox _rememberLogin;
    @InjectView(R.id.btn_login)
    Button _loginButton;

    private ProgressDialog progressDialog = null;
    private SharedPreferences settings = null;
    private String usr;
    private String passwd;

    public static Context getAppContext() {
        return context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.inject(this);

        context = getApplicationContext();
        progressDialog = new ProgressDialog(LoginActivity.this, R.style.AppTheme_Dialog);
        settings = getSharedPreferences("CPP_APP_PREFs", MODE_PRIVATE);

        // Crypto helper class
        crypt = new LoginCrypto();
        crypt.LoginCrypto();

        onLoginSuccess();

        try {
            if (crypt.keyStore.containsAlias("CharlesSpencerAPPusr") && crypt.keyStore.containsAlias("CharlesSpencerAPPpasswd")
                    && settings.contains("usr") && settings.contains("passwd")) {

                ifNetworkOKLogin(false);
            }
        } catch (Exception e) {
            Log.e(TAG, Log.getStackTraceString(e));
        }

        _rememberLogin.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    settings.edit().putBoolean("RememberCredentials", true).apply();
                } else {
                    settings.edit().putBoolean("RememberCredentials", false).apply();
                }
            }
        });

        _loginButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (!validate()) {
                    onLoginFailed();
                    return;
                }
                ifNetworkOKLogin(true);
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        progressDialog.cancel();
    }

    /*
        Our main login function
        -----------------------
        May be called either directly, or in the case of a new user entry,
        called from our updateCredentials() function which retrieves any existing
        user credentials
     */
    public void login(Boolean newUsr) {
        Log.d(TAG, "Login");

        _loginButton.setEnabled(false);

        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Authenticating...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        // Are we a new user?
        if (newUsr) {
            usr = _usernameText.getText().toString();
            passwd = _passwordText.getText().toString();

            // Were we asked to remember the users login?
            if (settings.getBoolean("RememberCredentials", false)) {
                try {
                    // Create keys with our helper class -- asynchronously
                    if (!crypt.keyStore.containsAlias("CharlesSpencerAPPusr") && !crypt.keyStore.containsAlias("CharlesSpencerAPPpasswd")) {
                        AsyncLoginTask asyncUsrKeyGen = new AsyncLoginTask();
                        AsyncLoginTask.delegate = this;
                        asyncUsrKeyGen.execute("genKey", "CharlesSpencerAPPusr");

                        AsyncLoginTask asyncPasswdKeyGen = new AsyncLoginTask();
                        AsyncLoginTask.delegate = this;
                        asyncPasswdKeyGen.execute("genKey", "CharlesSpencerAPPpasswd");
                    } else { // User was just logged out -- we already have keys generated for this phone
                        storeCredentials();
                    }
                } catch (Exception e) {
                    Log.e(TAG, Log.getStackTraceString(e));
                }
            }
        }

        // Attempt to execute the login with our helper async class
        // Delegate the response to us -- will be returned
        AsyncLoginTask async = new AsyncLoginTask();
        AsyncLoginTask.delegate = this;
        async.execute("doLogin", DISTRICT_ADDR, usr, passwd);
    }

    /*
        Delayed authorizing popup box to show the user
        something is happening in the background
     */
    public void loginTask(Boolean validity) {

        class loginDialog implements Runnable {
            boolean loginValid;

            public loginDialog(Boolean validity) {
                this.loginValid = validity;
            }

            public void run() {
                if (loginValid) {
                    onLoginSuccess();
                } else {
                    progressDialog.dismiss();
                    SharedPreferences.Editor editor = settings.edit();

                    // Remove the stored encrypted strings
                    // (Should only happen if we used a bad password user combo -- as we checked
                    //  other parameters such as the internet condition prior to this)
                    settings.edit().remove("usr");
                    settings.edit().remove("passwd");
                    editor.commit();

                    onLoginFailed();
                }
            }
        }

        new android.os.Handler().postDelayed(new loginDialog(validity), 2000);
    }

    /*
        Callback from our login procedure
     */
    @Override
    public void loginFinished(Boolean validity) {
        loginTask(validity);
    }

    /*
        Callback from our keygen procedure
     */
    @Override
    public void genKeyFinished(Boolean result) {
        try {
            if (result && crypt.keyStore.containsAlias("CharlesSpencerAPPusr") && crypt.keyStore.containsAlias("CharlesSpencerAPPpasswd")) { // Check that it actually worked...
                // Continue onto storing the credentials the user entered -- encrypting
                // them with the keys we just generated
                storeCredentials();
            }
        } catch (Exception e) {
            Log.e(TAG, e.getStackTrace().toString());
        }
    }

    @Override
    public void onBackPressed() {
        // Disable going back to the MainActivity
        moveTaskToBack(true);
    }

    /*
        Called on login success
     */
    public void onLoginSuccess() {
        // Move to our main activity
        Intent mainActivity = new Intent(this, MainActivity.class);
        startActivity(mainActivity);
        finish();
    }

    /*
        Called on Login failed
     */
    public void onLoginFailed() {
        Snackbar s = Snackbar.make(findViewById(android.R.id.content), "Login Failed", Snackbar.LENGTH_LONG);
        View snackView = s.getView();
        snackView.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.red));
        TextView snackTextView = (TextView) snackView.findViewById(android.support.design.R.id.snackbar_text);

        snackTextView.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);

        s.show();

        _loginButton.setEnabled(true);
    }

    /*
        Validate that the credentials given to us
        are valid for use in our pcas Login attempt
     */
    public boolean validate() {
        boolean valid = true;

        String username = _usernameText.getText().toString();
        String password = _passwordText.getText().toString();

        if (username.isEmpty()) {
            _usernameText.setError("Enter a valid username.");
            valid = false;
        } else {
            _usernameText.setError(null);
        }

        if (password.isEmpty() || password.length() < 3) {
            _passwordText.setError("Must be more than three alphanumeric characters.");
            valid = false;
        } else {
            _passwordText.setError(null);
        }

        return valid;
    }

    /*
        Update the usr and passwd variables use to login,
        also update the physical appearance of each text box
     */
    private void updateCredentials() {
        usr = crypt.decryptStr("CharlesSpencerAPPusr", settings.getString("usr", ""));
        passwd = crypt.decryptStr("CharlesSpencerAPPpasswd", settings.getString("passwd", ""));

        _usernameText.setText(usr);
        _passwordText.setText(passwd);

        _rememberLogin.setChecked(true);
    }

    /*
        Store the credentials given to us in our shared preferences as encrypted strings
        (Assuming they passed the initial validity check -- these are removed if the server
        responds with something other than a status OK)
     */
    public void storeCredentials() {
        String encryptedUsr = crypt.encryptStr("CharlesSpencerAPPusr", usr);
        String encryptedPasswd = crypt.encryptStr("CharlesSpencerAPPpasswd", passwd);

        SharedPreferences.Editor editor = settings.edit();
        editor.putString("usr", encryptedUsr);
        editor.putString("passwd", encryptedPasswd);
        editor.apply();
    }

    /*
        Check if we have a valid network connection
        and alert the user if we are on mobile data --
        then call our login function
     */
    public void ifNetworkOKLogin(final Boolean newUsr) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();

        if (!newUsr) {
            updateCredentials();
        }

        if (activeNetwork != null) {
            if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) { // We're on mobile data
                DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == DialogInterface.BUTTON_POSITIVE) {
                            if (newUsr) {
                                login(true);
                            } else {
                                login(false);
                            }
                        }
                    }
                };
                AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(LoginActivity.this, R.style.AppTheme_PopupOverlay));
                builder.setMessage("You're on mobile data. Do you wish to continue?").setPositiveButton("Yes", dialogClickListener)
                        .setNegativeButton("No", dialogClickListener).show();
            } else { // Good to go
                if (newUsr) {
                    login(true);
                } else {
                    updateCredentials();
                    login(false);
                }
            }
        } else {
            Snackbar internetBar = Snackbar.make(findViewById(android.R.id.content), "No Internet Connection", Snackbar.LENGTH_LONG);

            View snackView = internetBar.getView();
            snackView.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.red));
            TextView snackTextView = (TextView) snackView.findViewById(android.support.design.R.id.snackbar_text);

            snackTextView.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);

            internetBar.show();
        }
    }
}
