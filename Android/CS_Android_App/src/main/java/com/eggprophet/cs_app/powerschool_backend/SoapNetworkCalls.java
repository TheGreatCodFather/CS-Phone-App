package com.eggprophet.cs_app.powerschool_backend;

import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.TimeZone;

import javax.net.ssl.SSLContext;

import com.eggprophet.cs_app.BuildConfig;
import com.eggprophet.cs_app.login.LoginActivity;
import com.eggprophet.cs_app.R;
import com.eggprophet.cs_app.powerschool_backend.data.data_models.UserSessionVO;
import cz.msebera.android.httpclient.HttpVersion;
import cz.msebera.android.httpclient.auth.AuthScope;
import cz.msebera.android.httpclient.auth.UsernamePasswordCredentials;
import cz.msebera.android.httpclient.client.config.RequestConfig;
import cz.msebera.android.httpclient.client.methods.CloseableHttpResponse;
import cz.msebera.android.httpclient.client.methods.HttpPost;
import cz.msebera.android.httpclient.config.SocketConfig;
import cz.msebera.android.httpclient.conn.ssl.NoopHostnameVerifier;
import cz.msebera.android.httpclient.conn.ssl.SSLConnectionSocketFactory;
import cz.msebera.android.httpclient.conn.ssl.TrustSelfSignedStrategy;
import cz.msebera.android.httpclient.entity.StringEntity;
import cz.msebera.android.httpclient.extras.Base64;
import cz.msebera.android.httpclient.impl.client.BasicCredentialsProvider;
import cz.msebera.android.httpclient.impl.client.CloseableHttpClient;
import cz.msebera.android.httpclient.impl.client.HttpClientBuilder;
import cz.msebera.android.httpclient.ssl.SSLContexts;
import cz.msebera.android.httpclient.util.EntityUtils;

public class SoapNetworkCalls {
    private static final String CONTENT_TYPE_JSON = "application/json";
    private static final String CONTENT_TYPE_XML = "text/xml; charset=utf-8";
    public static final int DEFAULT_CONNECTION_TIMEOUT_MILLISECONDS = 180000;
    private static final String JSON_ISO8601_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
    private static final String PEARSON_REST_SERVICE_PATH = "/pearson-rest/services/";
    private static final String PUBLICPORTALSERVICE_NAMESPACE_ATTRIBUTE = "\"http://publicportal.rest.powerschool.pearson.com/xsd\"";
    private static final String PUBLIC_PORTAL_SERVICE_JSON = "PublicPortalServiceJSON";
    private static final String SOAP_BODY_END_TAG = "</soap:Body>";
    private static final String SOAP_BODY_START_TAG = "<soap:Body>";
    private static final String SOAP_ENVELOPE_END_TAG = "</soap:Envelope>";
    private static final String SOAP_ENVELOPE_START_TAG_FRAGMENT = "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"  xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\"";
    private static final String SOAP_HEADER_END_TAG = "</soap:Header>";
    private static final String SOAP_HEADER_START_TAG = "<soap:Header>";
    private static final String SOAP_SECURITY_HEADER_CREATED_END_TAG = "</wsu:Created>";
    private static final String SOAP_SECURITY_HEADER_CREATED_START_TAG = "<wsu:Created>";
    private static final String SOAP_SECURITY_HEADER_END_TAG = "</wsse:Security>";
    private static final String SOAP_SECURITY_HEADER_NONCE_END_TAG = "</wsse:Nonce>";
    private static final String SOAP_SECURITY_HEADER_NONCE_START_TAG = "<wsse:Nonce>";
    private static final String SOAP_SECURITY_HEADER_PASSWORD_END_TAG = "</wsse:Password>";
    private static final String SOAP_SECURITY_HEADER_PASSWORD_START_TAG = "<wsse:Password Type=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordDigest\">";
    private static final String SOAP_SECURITY_HEADER_START_TAG = "<wsse:Security xmlns:wsse=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\" soap:mustUnderstand=\"1\">";
    private static final String SOAP_SECURITY_HEADER_USERNAME_END_TAG = "</wsse:Username>";
    private static final String SOAP_SECURITY_HEADER_USERNAME_START_TAG = "<wsse:Username>";
    private static final String SOAP_SECURITY_HEADER_USERNAME_TOKEN_END_TAG = "</wsse:UsernameToken>";
    private static final String SOAP_SECURITY_HEADER_USERNAME_TOKEN_START_TAG = "<wsse:UsernameToken>";
    private static final String TAG = "SoapNetworkCalls";
    private static final String XML_DECLARATION = "<?xml version=\"1.0\" encoding=\"utf-8\"?>";

    public static String login(String districtServerAddress, String userName, String password, int userType) {
        String methodName = "login";
        CloseableHttpClient httpClient = getHttpClient();
        try {
            HttpPost httpPost = getHttpPost(districtServerAddress, PUBLIC_PORTAL_SERVICE_JSON, methodName);
            StringBuilder loginXml = getSoapEnvelopeStartFragment(methodName, PUBLICPORTALSERVICE_NAMESPACE_ATTRIBUTE).append("<username><![CDATA[").append(userName).append("]]></username>").append("<password><![CDATA[").append(password).append("]]></password>").append("<userType>").append(userType).append("</userType>").append(getSoapEnvelopeEndFragment(methodName));
            Log.d(TAG, methodName + " request xml " + loginXml);
            String executeSoapRequest = executeSoapRequest(methodName, httpClient, httpPost, loginXml);
            return executeSoapRequest;
        } finally {
            try {
                httpClient.close();
            } catch (IOException e) {
                Log.e(TAG, e.getMessage());
            }
        }
    }

    public static String loginToPublicPortal(String districtServerAddress, String userName, String password) {
        String methodName = "loginToPublicPortal";
        CloseableHttpClient httpClient = getHttpClient();
        try {
            HttpPost httpPost = getHttpPost(districtServerAddress, PUBLIC_PORTAL_SERVICE_JSON, methodName);
            StringBuilder loginXml = getSoapEnvelopeStartFragment(methodName, PUBLICPORTALSERVICE_NAMESPACE_ATTRIBUTE).append("<username><![CDATA[").append(userName).append("]]></username>").append("<password><![CDATA[").append(password).append("]]></password>").append(getSoapEnvelopeEndFragment(methodName));
            Log.d(TAG, methodName + " request xml " + loginXml);
            String executeSoapRequest = executeSoapRequest(methodName, httpClient, httpPost, loginXml);
            return executeSoapRequest;
        } finally {
            try {
                httpClient.close();
            } catch (IOException e) {
                Log.e(TAG, e.getMessage());
            }
        }
    }

    public static String linkDeviceTokenToUser(String districtServerAddress, UserSessionVO userSession, String deviceToken) {
        String response = null;
        String methodName = "linkDeviceTokenToUser";
        CloseableHttpClient httpClient = getHttpClient();
        try {
            HttpPost httpPost = getHttpPost(districtServerAddress, PUBLIC_PORTAL_SERVICE_JSON, methodName);
            StringBuilder deviceTokenXml = getSoapEnvelopeStartFragment(methodName, PUBLICPORTALSERVICE_NAMESPACE_ATTRIBUTE);
            serializeUserSession(userSession, deviceTokenXml);
            deviceTokenXml.append("<deviceToken>").append(deviceToken).append("</deviceToken>");
            deviceTokenXml.append(getSoapEnvelopeEndFragment(methodName));
            Log.d(TAG, methodName + " request xml " + deviceTokenXml);
            response = executeSoapRequest(methodName, httpClient, httpPost, deviceTokenXml);
            return response;
        } finally {
            try {
                httpClient.close();
            } catch (IOException e) {
                Log.e(TAG, e.getMessage());
            }
        }
    }

    public static String logout(String districtServerAddress, UserSessionVO userSession) {
        String response = null;
        String methodName = "logout";
        CloseableHttpClient httpClient = getHttpClient();
        try {
            HttpPost httpPost = getHttpPost(districtServerAddress, PUBLIC_PORTAL_SERVICE_JSON, methodName);
            StringBuilder logoutXml = getSoapEnvelopeStartFragment(methodName, PUBLICPORTALSERVICE_NAMESPACE_ATTRIBUTE);
            serializeUserSession(userSession, logoutXml);
            logoutXml.append(getSoapEnvelopeEndFragment(methodName));
            Log.d(TAG, methodName + " request xml " + logoutXml);
            response = executeSoapRequest(methodName, httpClient, httpPost, logoutXml);
            return response;
        } finally {
            try {
                httpClient.close();
            } catch (IOException e) {
                Log.e(TAG, e.getMessage());
            }
        }
    }

    public static String logoutAndDelinkDeviceToken(String districtServerAddress, UserSessionVO userSession, String deviceToken) {
        String response = null;
        String methodName = "logoutAndDelinkDeviceToken";
        CloseableHttpClient httpClient = getHttpClient();
        try {
            HttpPost httpPost = getHttpPost(districtServerAddress, PUBLIC_PORTAL_SERVICE_JSON, methodName);
            StringBuilder logoutXml = getSoapEnvelopeStartFragment(methodName, PUBLICPORTALSERVICE_NAMESPACE_ATTRIBUTE);
            serializeUserSession(userSession, logoutXml);
            logoutXml.append("<deviceToken>").append(deviceToken).append("</deviceToken>");
            logoutXml.append(getSoapEnvelopeEndFragment(methodName));
            Log.d(TAG, methodName + " request xml " + logoutXml);
            response = executeSoapRequest(methodName, httpClient, httpPost, logoutXml);
            return response;
        } finally {
            try {
                httpClient.close();
            } catch (IOException e) {
                Log.e(TAG, e.getMessage());
            }
        }
    }

    private static CloseableHttpClient getHttpClient(boolean includeCredentials, int connectionTimeOutInMilliSeconds) {
        HttpClientBuilder httpClientBuilder = HttpClientBuilder.create();
        httpClientBuilder.setDefaultRequestConfig(RequestConfig.custom().setConnectTimeout(connectionTimeOutInMilliSeconds).build());
        httpClientBuilder.setDefaultSocketConfig(SocketConfig.custom().setTcpNoDelay(true).build());

        try {
            CertificateFactory cf = CertificateFactory.getInstance("X.509");
            InputStream caInput = LoginActivity.getAppContext().getResources().openRawResource(R.raw.gppsdabca);
            Certificate ca;
            try {
                ca =cf.generateCertificate(caInput);
                System.out.println("ca=" + ((X509Certificate) ca).getSubjectDN());
            } finally {
                caInput.close();
            }
            KeyStore ks = KeyStore.getInstance(KeyStore.getDefaultType());
            ks.load(null, null);
            ks.setCertificateEntry("ca", ca);

            SSLContext sslContext = SSLContexts.custom().useProtocol("TLS").loadTrustMaterial(ks, new TrustSelfSignedStrategy()).build();
            SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(sslContext,
                    new String[] { "TLSv1" }, null, new NoopHostnameVerifier());

            httpClientBuilder.setSSLSocketFactory(sslsf);
        } catch(CertificateException e) {
            Log.e(TAG, e.getMessage());
        } catch(KeyStoreException e) {
            Log.e(TAG, e.getMessage());
        } catch(NoSuchAlgorithmException e) {
            Log.e(TAG, e.getMessage());
        } catch(KeyManagementException e) {
            Log.e(TAG, e.getMessage());
        } catch(IOException e) {
            Log.e(TAG, e.getMessage());
        }

        if (includeCredentials) {
            BasicCredentialsProvider credentialsProvider = new BasicCredentialsProvider();
            credentialsProvider.setCredentials(AuthScope.ANY, new UsernamePasswordCredentials("pearson", "m0bApP5"));
            httpClientBuilder.setDefaultCredentialsProvider(credentialsProvider);
        }
        return httpClientBuilder.build();
    }

    private static CloseableHttpClient getHttpClient() {
        return getHttpClient(true, DEFAULT_CONNECTION_TIMEOUT_MILLISECONDS);
    }

    private static HttpPost getHttpPost(String districtAddress, String serviceName, String methodName, boolean jsonResponse) {
        String servicePath = districtAddress + PEARSON_REST_SERVICE_PATH;
        Log.d(TAG, "Service path: " + servicePath);
        StringBuilder serviceUri = new StringBuilder().append(servicePath).append(serviceName);
        if (jsonResponse) {
            serviceUri.append("?response=").append(CONTENT_TYPE_JSON);
        }
        HttpPost httpPost = new HttpPost(serviceUri.toString());
        httpPost.addHeader("Content-type", CONTENT_TYPE_XML);
        httpPost.addHeader("SOAPAction", serviceName + "#" + methodName);
        httpPost.setProtocolVersion(HttpVersion.HTTP_1_1);
        return httpPost;
    }

    private static HttpPost getHttpPost(String districtAddress, String serviceName, String methodName) {
        return getHttpPost(districtAddress, serviceName, methodName, true);
    }

    private static String executeSoapRequest(String requestIdentifier, CloseableHttpClient httpClient, HttpPost httpPost, StringBuilder requestBody) {
        try {
            long operationStartTime = System.currentTimeMillis();
            StringEntity entity = new StringEntity(requestBody.toString(), StandardCharsets.UTF_8);
            entity.setContentType(CONTENT_TYPE_XML);
            httpPost.setEntity(entity);
            CloseableHttpResponse httpResponse = httpClient.execute(httpPost);
            long networkCallDuration = System.currentTimeMillis() - operationStartTime;
            operationStartTime = System.currentTimeMillis();
            int responseCode = httpResponse.getStatusLine().getStatusCode();
            String response = EntityUtils.toString(httpResponse.getEntity());
            long responseIODuration = System.currentTimeMillis() - operationStartTime;
            long totalServiceCallDuration = networkCallDuration + responseIODuration;
            StringBuilder performanceStatBuilder = new StringBuilder();
            performanceStatBuilder.append("Network Performance stats for " + requestIdentifier + ":\n").append("Network call duration: ").append(networkCallDuration).append("\n").append("Response i/o duration: ").append(responseIODuration).append("\n").append("Total Service Call duration: ").append(totalServiceCallDuration);
            Log.d(TAG, performanceStatBuilder.toString());
            if (responseCode < 300) {
                httpResponse.close();
                Log.e(TAG, response);
                Log.e(TAG, "Server responded: " + responseCode);
                return response;
            }
            Log.e(TAG, "Non 2xx response code for " + requestIdentifier + ": " + responseCode);
            Log.e(TAG, "SOAP response with non 2xx response code for  " + requestIdentifier + ": " + response);
        } catch (IOException ioEx) {
            Log.e(TAG, "Network call to " + requestIdentifier + " failed", ioEx);
        }
        return null;
    }

    public static String getStudentData(String districtServerAddress, UserSessionVO userSession, List<Long> studentIDs, List<Integer> includeList) {
        String response = null;
        String methodName = "getStudentData";
        CloseableHttpClient httpClient = getHttpClient();
        try {
            HttpPost httpPost = getHttpPost(districtServerAddress, PUBLIC_PORTAL_SERVICE_JSON, methodName);
            StringBuilder studentDataXml = getSoapEnvelopeStartFragment(methodName, PUBLICPORTALSERVICE_NAMESPACE_ATTRIBUTE);
            serializeUserSession(userSession, studentDataXml);
            serializeStudentIDs(studentIDs, studentDataXml);
            serializeQueryIncludeList(includeList, studentDataXml);
            studentDataXml.append(getSoapEnvelopeEndFragment(methodName));
            Log.d(TAG, methodName + " request xml " + studentDataXml);
            response = executeSoapRequest(methodName, httpClient, httpPost, studentDataXml);
            return response;
        } finally {
            try {
                httpClient.close();
            } catch (IOException e) {
                Log.e(TAG, e.getMessage());
            }
        }
    }

    public static String getStudentPhoto(String districtServerAddress, UserSessionVO userSession, long studentID) {
        String response = null;
        String methodName = "getStudentPhoto";
        CloseableHttpClient httpClient = getHttpClient();
        try {
            HttpPost httpPost = getHttpPost(districtServerAddress, PUBLIC_PORTAL_SERVICE_JSON, methodName);
            StringBuilder studentDataXml = getSoapEnvelopeStartFragment(methodName, PUBLICPORTALSERVICE_NAMESPACE_ATTRIBUTE);
            serializeUserSession(userSession, studentDataXml);
            serializeStudentID(studentID, studentDataXml);
            studentDataXml.append(getSoapEnvelopeEndFragment(methodName));
            Log.d(TAG, methodName + " request xml " + studentDataXml);
            response = executeSoapRequest(methodName, httpClient, httpPost, studentDataXml);
            return response;
        } finally {
            try {
                httpClient.close();
            } catch (IOException e) {
                Log.e(TAG, e.getMessage());
            }
        }
    }

    private static StringBuilder getSoapEnvelopeEndFragment(String methodName) {
        return new StringBuilder().append("</").append(methodName).append(">").append(SOAP_BODY_END_TAG).append(SOAP_ENVELOPE_END_TAG);
    }

    private static StringBuilder getSoapEnvelopeStartFragment(String methodName, String namespace, boolean includeSecurityHeader) {
        StringBuilder soapEnvolopeStartFragment = new StringBuilder().append(XML_DECLARATION).append(SOAP_ENVELOPE_START_TAG_FRAGMENT).append(" xmlns=").append(namespace).append(">");
        if (includeSecurityHeader) {
            soapEnvolopeStartFragment.append(SOAP_HEADER_START_TAG);
            serializeSOAPSecurityHeader("pearson", "m0bApP5", soapEnvolopeStartFragment);
            soapEnvolopeStartFragment.append(SOAP_HEADER_END_TAG);
        }
        soapEnvolopeStartFragment.append(SOAP_BODY_START_TAG).append("<").append(methodName).append(" xmlns=").append(namespace).append(">");
        return soapEnvolopeStartFragment;
    }

    private static StringBuilder getSoapEnvelopeStartFragment(String methodName, String namespace) {
        return getSoapEnvelopeStartFragment(methodName, namespace, false);
    }

    private static void serializeQueryIncludeList(List<Integer> includeList, StringBuilder requestXML) {
        requestXML.append("<qil>");
        for (Integer include : includeList) {
            requestXML.append("<includes>").append(include).append("</includes>");
        }
        requestXML.append("</qil>");
    }

    private static void serializeStudentID(long studentID, StringBuilder requestXML) {
        requestXML.append("<studentID>").append(studentID).append("</studentID>");
    }

    private static void serializeStudentIDs(List<Long> studentIDs, StringBuilder requestXML) {
        if (studentIDs == null || studentIDs.isEmpty()) {
            requestXML.append("<studentIDs/>");
            return;
        }
        for (Long studentId : studentIDs) {
            requestXML.append("<studentIDs>").append(studentId).append("</studentIDs>");
        }
    }

    private static void serializeUserSession(UserSessionVO userSession, StringBuilder requestXML) {
        requestXML.append("<userSessionVO><userId>").append(userSession.getUserId()).append("</userId><serviceTicket>").append(userSession.getServiceTicket()).append("</serviceTicket>");
        if (userSession.getStudentIDs() != null) {
            serializeStudentIDs(userSession.getStudentIDs(), requestXML);
        }
        if (userSession.getServerInfo() != null) {
            requestXML.append("<serverInfo><apiVersion><![CDATA[").append(userSession.getServerInfo().getApiVersion()).append("]]></apiVersion></serverInfo>");
        }
        if (userSession.getServerCurrentTime() != null) {
            requestXML.append("<serverCurrentTime>").append(userSession.getServerCurrentTime()).append("</serverCurrentTime>");
        }
        requestXML.append("<userType>").append(userSession.getUserType()).append("</userType>");
        requestXML.append("</userSessionVO>");
    }

    private static void serializeSOAPSecurityHeader(String userName, String password, StringBuilder requesXML) {
        try {
            String nonce = getNonce();
            Date now = new Date();
            SimpleDateFormat dateFormat = new SimpleDateFormat(JSON_ISO8601_DATE_FORMAT);
            dateFormat.setTimeZone(TimeZone.getTimeZone("gmt"));
            String createdTimeStamp = dateFormat.format(now);
            requesXML.append(SOAP_SECURITY_HEADER_START_TAG).append(SOAP_SECURITY_HEADER_USERNAME_TOKEN_START_TAG).append(SOAP_SECURITY_HEADER_USERNAME_START_TAG).append(userName).append(SOAP_SECURITY_HEADER_USERNAME_END_TAG).append(SOAP_SECURITY_HEADER_PASSWORD_START_TAG).append(getPasswordDigestForSoapSecurityHeader(nonce, createdTimeStamp, password)).append(SOAP_SECURITY_HEADER_PASSWORD_END_TAG).append(SOAP_SECURITY_HEADER_NONCE_START_TAG).append(Base64.encodeToString(getUTF8EncodedBytes(hexEncode(nonce)), 2)).append(SOAP_SECURITY_HEADER_NONCE_END_TAG).append(SOAP_SECURITY_HEADER_CREATED_START_TAG).append(createdTimeStamp).append(SOAP_SECURITY_HEADER_CREATED_END_TAG).append(SOAP_SECURITY_HEADER_USERNAME_TOKEN_END_TAG).append(SOAP_SECURITY_HEADER_END_TAG);
        } catch (Exception ex) {
            Log.e(TAG, "Could not add security header to the soap request. This will lead to soap request failiure", ex);
        }
    }

    private static String getPasswordDigestForSoapSecurityHeader(String nonce, String createdTimeStamp, String password) {
        String encodedPasswordDigest = null;
        try {
            encodedPasswordDigest = Base64.encodeToString(getMessageDigest(getUTF8EncodedBytes(hexEncode(nonce) + createdTimeStamp + password), "SHA1"), 2);
        } catch (Exception ex) {
            Log.e(TAG, "Error computing password digest for Soap Security Header. Will be returning null", ex);
        }
        return encodedPasswordDigest;
    }

    private static String hexEncode(String source) {
        StringBuilder sb = new StringBuilder(BuildConfig.VERSION_NAME);
        for (int i = 0; i < (source.length() - 2) + 1; i += 2) {
            sb.append((char) Integer.parseInt(source.substring(i, i + 2), 16));
        }
        return sb.toString();
    }

    private static byte[] getUTF8EncodedBytes(String string) throws UnsupportedEncodingException {
        return string.getBytes(StandardCharsets.UTF_8);
    }

    private static byte[] getMessageDigest(byte[] sourceBytes, String algorithm) throws Exception {
        return MessageDigest.getInstance(algorithm).digest(sourceBytes);
    }

    public static String getNonce() {
        return String.valueOf(new Random().nextInt(999999999));
    }

}