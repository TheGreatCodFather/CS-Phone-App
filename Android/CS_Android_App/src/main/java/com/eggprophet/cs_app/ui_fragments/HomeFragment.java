package com.eggprophet.cs_app.ui_fragments;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.util.LruCache;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.util.Xml;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import org.apache.commons.lang3.StringEscapeUtils;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import com.eggprophet.cs_app.R;
import com.eggprophet.cs_app.rss.RssFeedListAdapter;
import com.eggprophet.cs_app.rss.RssFeedModel;

public class HomeFragment extends Fragment {

    private static final String TAG = "HomeFragment";

    private RecyclerView mRecyclerView;
    private SwipeRefreshLayout mSwipeLayout;
    private MenuItem mRefreshItem;

    private List<RssFeedModel> mFeedModelList;
    RssFeedListAdapter mAdapter;

    ConnectivityManager cm = null;


    /**
     * Memory cache for stored images
     */

    private LruCache<String, Bitmap> mImageCache;

    // Get max available VM memory, exceeding this amount will throw an
    // OutOfMemory exception. Stored in kilobytes as LruCache takes an
    // int in its constructor.
    final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);

    // Use 1/8th of the available memory for this memory cache.
    final int cacheSize = maxMemory / 8;

    int cachedImgsCount = 0;




    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Retain this fragment instance
        setRetainInstance(true);

        // We're overriding our options menu
        setHasOptionsMenu(true);

        // Initialize the cache for our images
        mImageCache = new LruCache<String, Bitmap>(cacheSize) {
            @Override
            protected int sizeOf(String key, Bitmap bitmap) {
                // The cache size will be measured in kilobytes rather than
                // number of items. -- We keep track of how many images we put
                // into the cache to receive the number of items
                return bitmap.getByteCount() / 1024;
            }
        };

        // Get our connectivity manager and check if we have a connection
        // -- if so -- start our fetch task for the RSS feed
        cm = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);

        if (cm.getActiveNetworkInfo() != null) {
            new FetchFeedTask().execute((Void) null);
        }
    }

    /**
     * FIXME -- We should really be inflating a separate layout
     * FIXME -- for this icon and any other ones that we alter programmatically
     * FIXME -- Setting visibility is hacky work-a-round
     *
     * @param menu
     * @param inflater
     */
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        mRefreshItem = menu.findItem(R.id.action_refresh);
        mRefreshItem.setVisible(true);
    }

    // Another way for the user to refresh is through our actionbar refresh icon
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_refresh) {
            if (mFeedModelList != null && cm.getActiveNetworkInfo() != null) { // No need to refresh if the network isn't active

                // Turn on our refresh icon animation in the actionbar
                activateRefreshIcon();

                // Clear our RSS feed model data set
                mFeedModelList.clear();

                // Evict previous images (reset cache)
                mImageCache.evictAll();

                // Notify our adaptor of a change in the data set
                mAdapter.notifyDataSetChanged();

                // Fetch our data again
                new FetchFeedTask().execute((Void) null);
            }
        }
        return super.onOptionsItemSelected(item);
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.home_fragment, container, false);

        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerView);
        mSwipeLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipeRefreshLayout);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        // Register the refresh listener for our SwipeRefreshLayout
        mSwipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (cm.getActiveNetworkInfo() != null) {

                    // Clear our RSS feed model data set
                    mFeedModelList.clear();

                    // Evict images in cache (reset cache)
                    mImageCache.evictAll();

                    // Notify our adaptor of a change in the data set
                    mAdapter.notifyDataSetChanged();

                    // Fetch our data again
                    new FetchFeedTask().execute((Void) null);

                } else { // Cancel refresh if their is no network connection as it's pointless
                    mSwipeLayout.setRefreshing(false);
                }
            }
        });

        // We are initially refreshing/loading data when we enter the fragment
        mSwipeLayout.setRefreshing(true);

        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        // FIXME shotty work-a-round
        mRefreshItem.setVisible(false);

        // Remove the animation if it is currently running
        if (mRefreshItem.getActionView() != null) {
            mRefreshItem.getActionView().clearAnimation();
            mRefreshItem.setActionView(null);
        }
    }


    public void addBitmapToMemoryCache(String key, Bitmap bitmap) {
        if (getBitmapFromMemCache(key) == null) {
            mImageCache.put(key, bitmap);
        }
    }

    public Bitmap getBitmapFromMemCache(String key) {
        return mImageCache.get(key);
    }

    /**
     * Parses returned data from our FetchFeedTask
     *
     * @param inputStream
     * @return RssFeedModel items
     * @throws XmlPullParserException
     * @throws IOException
     */
    public List<RssFeedModel> parseFeed(InputStream inputStream) throws XmlPullParserException, IOException {
        String title = null;
        String link = null;
        String description = null;
        boolean isItem = false;
        List<RssFeedModel> items = new ArrayList<>();

        if (inputStream != null) {
            try {
                XmlPullParser xmlPullParser = Xml.newPullParser();
                xmlPullParser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
                xmlPullParser.setInput(inputStream, null);

                int i = 0;

                while (xmlPullParser.next() != XmlPullParser.END_DOCUMENT) {

                    int eventType = xmlPullParser.getEventType();

                    String name = xmlPullParser.getName();
                    if (name == null)
                        continue;

                    if (eventType == XmlPullParser.END_TAG) {
                        if (name.equalsIgnoreCase("item")) {
                            isItem = false;
                        }
                        continue;
                    }

                    if (eventType == XmlPullParser.START_TAG) {
                        if (name.equalsIgnoreCase("enclosure")) {
                            new DownloadImageTask(i).execute(xmlPullParser.getAttributeValue(null, "url"));
                            i++;
                        } else if (name.equalsIgnoreCase("item")) {
                            isItem = true;
                            continue;
                        }
                    }

                    Log.d("MainActivity", "Parsing name --> " + name);
                    String result = "";
                    if (xmlPullParser.next() == XmlPullParser.TEXT) {
                        result = StringEscapeUtils.unescapeHtml4(StringEscapeUtils.unescapeXml(xmlPullParser.getText()));
                    }

                    if (name.equalsIgnoreCase("title")) {
                        title = result;
                    } else if (name.equalsIgnoreCase("link")) {
                        link = result;
                    } else if (name.equalsIgnoreCase("description")) {
                        description = result;
                    }


                    if (title != null && link != null && description != null) {
                        if (isItem) {
                            RssFeedModel item = new RssFeedModel(title, link, description);
                            items.add(item);
                        }

                        title = null;
                        link = null;
                        description = null;
                        isItem = false;
                    }
                }
                cachedImgsCount = i;
                return items;
            } finally {
                inputStream.close();
            }
        }
        return null;
    }

    /**
     * Function that creates and animates a rotating imageview
     * in the action bar
     */
    public void activateRefreshIcon() {
        // Attach a rotating ImageView to the refresh item as an ActionView
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        ImageView refreshIV = (ImageView) inflater.inflate(R.layout.action_refresh, null);

        // Animate said imageview
        Animation rotation = AnimationUtils.loadAnimation(getActivity(), R.anim.action_refresh_anim);
        rotation.setRepeatCount(Animation.INFINITE);
        refreshIV.startAnimation(rotation);

        mRefreshItem.setActionView(refreshIV);
    }


    /**
     * Our async task for fetching the RSS feed
     */
    private class FetchFeedTask extends AsyncTask<Void, Void, Boolean> {

        private String urlLink;

        @Override
        protected void onPreExecute() {
            urlLink = "http://www.ctvnews.ca/rss/ctvnews-ca-top-stories-public-rss-1.822009"; // Use this news feed for now
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            if (TextUtils.isEmpty(urlLink))
                return false;

            try {
                if (!urlLink.startsWith("http://") && !urlLink.startsWith("https://"))
                    urlLink = "http://" + urlLink;

                URL url = new URL(urlLink);
                InputStream inputStream = null;
                try {
                    inputStream = url.openConnection().getInputStream();
                } catch (Exception e) {

                }
                mFeedModelList = parseFeed(inputStream);
                return true;
            } catch (IOException e) {
                Log.e(TAG, "Error", e);
            } catch (XmlPullParserException e) {
                Log.e(TAG, "Error", e);
            }
            return false;
        }

    }


    /**
     * Our async task for fetching and adding images to our cache
     */
    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        int pos;


        public DownloadImageTask(int pos) {
            this.pos = pos;
        }

        protected Bitmap doInBackground(String... urls) {
            String urlDisplay = urls[0];
            Bitmap bitmap = null;
            try {
                InputStream in = new java.net.URL(urlDisplay).openStream();
                bitmap = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return bitmap;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {

            addBitmapToMemoryCache(String.valueOf(pos), bitmap);

            /**
             * We populate the data here as our images are going to be the last to load...
             */
            if (pos == cachedImgsCount - 1) { // If this is the last image...
                mSwipeLayout.setRefreshing(false);

                if (mRefreshItem.getActionView() != null) {
                    // Remove the animation for our actionbar
                    mRefreshItem.getActionView().clearAnimation();
                    mRefreshItem.setActionView(null);
                }

                // Fill RecyclerView
                mAdapter = new RssFeedListAdapter(getActivity(), mFeedModelList, mImageCache);
                mRecyclerView.setAdapter(mAdapter);
                mAdapter.notifyDataSetChanged();
            }
        }
    }
}