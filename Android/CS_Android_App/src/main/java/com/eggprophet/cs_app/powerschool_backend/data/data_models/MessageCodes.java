package com.eggprophet.cs_app.powerschool_backend.data.data_models;

public class MessageCodes {
    public static final int CODE_ACCOUNT_DISABLED = 13;
    public static final int CODE_ASMT_BASED_WEIGHT_EXCEPTION = 15;
    public static final int CODE_COMMUNICATION_ERROR = 101;
    public static final int CODE_CONNECTION_ERROR = 102;
    public static final int CODE_DEVICE_TOKEN_LINK_SUCCESS = 30;
    public static final int CODE_DISTRICT_CODE_VALIDATION_UNAVAILABLE_ERROR = 103;
    public static final int CODE_HTTP_BAD_GATEWAY = 502;
    public static final int CODE_HTTP_BAD_REQUEST = 400;
    public static final int CODE_HTTP_CONFLICT = 409;
    public static final int CODE_HTTP_EXPECTATION_FAILED = 417;
    public static final int CODE_HTTP_FORBIDDEN = 403;
    public static final int CODE_HTTP_GATEWAY_TIMEOUT = 504;
    public static final int CODE_HTTP_GONE = 410;
    public static final int CODE_HTTP_INTERNAL_SERVER_ERROR = 500;
    public static final int CODE_HTTP_LENGTH_REQUIRED = 411;
    public static final int CODE_HTTP_METHOD_NOT_ALLOWED = 405;
    public static final int CODE_HTTP_NOT_ACCEPTABLE = 406;
    public static final int CODE_HTTP_NOT_FOUND = 404;
    public static final int CODE_HTTP_NOT_IMPLEMENTED = 501;
    public static final int CODE_HTTP_PRECONDITION_FAILED = 412;
    public static final int CODE_HTTP_PROXY_AUTHENTICATION_REQUIRED = 407;
    public static final int CODE_HTTP_REQUESTED_HTTP_RANGE_NOT_SATISFIABLE = 416;
    public static final int CODE_HTTP_REQUEST_ENTITY_TOO_LARGE = 413;
    public static final int CODE_HTTP_REQUEST_TIMEOUT = 408;
    public static final int CODE_HTTP_REQUEST_URI_TOO_LONG = 414;
    public static final int CODE_HTTP_SERVICE_UNAVAILABLE = 503;
    public static final int CODE_HTTP_UNAUTHORIZED = 401;
    public static final int CODE_HTTP_UNSUPPORTED_MEDIA_TYPE = 415;
    public static final int CODE_HTTP_VERSION_NOT_SUPPORTED = 505;
    public static final int CODE_INVALID_LOGIN = 1;
    public static final int CODE_INVALID_PASSWORD_RECOVERY_CREDENTIALS = 23;
    public static final int CODE_INVALID_RECOVERY_EMAIL = 21;
    public static final int CODE_INVALID_REPLY_TO_RECOVERY_EMAIL = 17;
    public static final int CODE_INVALID_SERVER_RESPONSE = 105;
    public static final int CODE_LOGIN_REQUIRED = 104;
    public static final int CODE_LOGOUT_FAILURE = 7;
    public static final int CODE_LOGOUT_SUCCESS = 6;
    public static final int CODE_MISCONFIGURED_EXTERNAL_SERVER_ADDRESS = 16;
    public static final int CODE_MISSING_RECOVERY_EMAIL = 20;
    public static final int CODE_MISSING_REPLY_TO_RECOVERY_EMAIL = 18;
    public static final int CODE_MISSING_USERNAME = 22;
    public static final int CODE_OLD_API_VERSION = 100;
    public static final int CODE_PASSWORD_APLHANUMERIC_VALIDATION_FAILURE = 26;
    public static final int CODE_PASSWORD_EXCEEDS_MAX_LENGTH = 24;
    public static final int CODE_PASSWORD_MINIMUM_LENGTH_FAILURE = 27;
    public static final int CODE_PASSWORD_MIXED_CASE_VALIDATION_FAILURE = 28;
    public static final int CODE_PASSWORD_NEEDS_RESET = 12;
    public static final int CODE_PASSWORD_REUSE_VIOLATION = 25;
    public static final int CODE_PASSWORD_SPECIAL_CHARACTER_VALIDATION_FAILURE = 29;
    public static final int CODE_PUBLIC_PORTAL_DISABLED = 32;
    public static final int CODE_RECOVERY_EMAIL_SENT = 19;
    public static final int CODE_REGISTRATION_CLOSED = 11;
    public static final int CODE_SAVE_NS_FAILURE = 9;
    public static final int CODE_SAVE_NS_SUCCESS = 8;
    public static final int CODE_SCHOOL_DISABLED = 3;
    public static final int CODE_SDK_BUSINESSLOGIC = 10;
    public static final int CODE_SERVICE_DISABLED = 2;
    public static final int CODE_SESSION_EXISTS = 5;
    public static final int CODE_SSO_DISABLED = 4;
    public static final int CODE_UNKNOWN = 0;
    public static final int CODE_UNSUPPORTED_USER_TYPE = 31;
    public static final int CODE_VALIDATION_FAILED = 14;
    private String description;
    private int msgCode;
    private String title;

    public String getDescription() {
        return this.description;
    }

    public int getMsgCode() {
        return this.msgCode;
    }

    public String getTitle() {
        return this.title;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setMsgCode(int msgCode) {
        this.msgCode = msgCode;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
