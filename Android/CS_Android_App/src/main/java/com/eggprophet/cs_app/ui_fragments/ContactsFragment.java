package com.eggprophet.cs_app.ui_fragments;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.support.v7.widget.PopupMenu;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

import com.eggprophet.cs_app.R;

public class ContactsFragment extends ListFragment {

    ArrayList<HashMap<String, String>> arraylist;
    JsoupListView asyncTask = new JsoupListView();

    MenuItem mSearchItem;

    // URL Address
    String url = "https://www.gppsd.ab.ca/school/charlesspencer/About/Pages/Staff-Directory.aspx";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setRetainInstance(true);
        setHasOptionsMenu(true);

        ConnectivityManager cm = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);

        if (cm.getActiveNetworkInfo() != null){
            // Execute DownloadJSON AsyncTask
            asyncTask.execute();
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        mSearchItem = menu.findItem(R.id.action_search);
        mSearchItem.setVisible(true);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        asyncTask.cancel(true);
        mSearchItem.setVisible(false);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);

        PopupMenu popup = new PopupMenu(getActivity(), v);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.contacts_popup, popup.getMenu());

        final int pos = position;

        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                ClipboardManager clipboard = (ClipboardManager) getActivity().getSystemService(getContext().CLIPBOARD_SERVICE);
                ClipData clip = null;

                switch (item.getItemId()) {
                    case R.id.contact_copy_name:
                        clip = ClipData.newPlainText("Name", arraylist.get(pos).get("name"));
                        clipboard.setPrimaryClip(clip);
                        break;
                    case R.id.contact_copy_email:
                        clip = ClipData.newPlainText("Email", arraylist.get(pos).get("email"));
                        clipboard.setPrimaryClip(clip);
                        break;
                    case R.id.contact_send_email:
                        Intent intent = new Intent(Intent.ACTION_SEND);
                        intent.setType("message/rfc822");
                        intent.putExtra(Intent.EXTRA_EMAIL, new String [] {arraylist.get(pos).get("email")});
                        try {
                            startActivity(Intent.createChooser(intent, "Send mail..."));
                        } catch (Exception e) {
                            Log.d("", "No email clients installed!");
                        }
                        break;
                }
                return false;
            }
        });

        popup.show();
    }

    // Title AsyncTask
    private class JsoupListView extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            // Create an array
            arraylist = new ArrayList<HashMap<String, String>>();

            try {
                // Connect to the Website URL
                Document doc = Jsoup.connect(url).get();
                // Identify Table Class
                for (Element table : doc.select("table[id=ctl00_SPWebPartManager1_g_1c15485f_46a5_43e5_a9fd_3a000ce8ab1c_ctl00_directoryGrid]")) {

                    // Identify all the table row's(tr)
                    for (Element row : table.select("tr:gt(0)")) {
                        HashMap<String, String> map = new HashMap<String, String>();

                        // Identify all the table cell's(td)
                        Elements tds = row.select("td");

                        if (!tds.get(0).text().contains("Test Portal")) {
                            // Retrive Jsoup Elements
                            // Get the first td
                            map.put("name", tds.get(0).text());
                            // Get the second td
                            map.put("jobTitle", tds.get(1).text());
                            // Get the third td
                            map.put("email", tds.get(2).text());
                            // Set all extracted Jsoup Elements into the array
                            arraylist.add(map);
                        }
                    }
                }
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            if(!isCancelled()) {
                Collections.sort(arraylist, new Comparator<HashMap<String, String>>() {

                    @Override
                    public int compare(HashMap<String, String> lhs,
                                       HashMap<String, String> rhs) {
                        String firstValue = lhs.get("name");
                        String secondValue = rhs.get("name");
                        return firstValue.compareTo(secondValue);
                    }
                });

                String[] from = new String[]{"name", "jobTitle", "email"};
                int[] to = new int[]{R.id.contacts_name, R.id.contacts_job_title, R.id.contacts_email};
                ListAdapter adapter = new SimpleAdapter(getContext(), arraylist, R.layout.contacts_list_item, from, to);
                setListAdapter(adapter);
            }
        }
    }
}
