package com.eggprophet.cs_app.ui_fragments;

/**
 * Created by eggprophet on 04/02/17.
 */


import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.VideoView;

import com.eggprophet.cs_app.R;
import com.eggprophet.cs_app.Util;

public class AboutFragment extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.about_fragment, container, false);

        setRetainInstance(true);

        final ImageButton github_button = (ImageButton) view.findViewById(R.id.about_gitlab_logo);
        github_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Util.openWebPage(getActivity(), "https://gitlab.com/TheGreatCodFather/CS-Phone-App");
            }
        });

        final ImageButton mit_button = (ImageButton) view.findViewById(R.id.about_mit_logo);
        mit_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Util.openWebPage(getActivity(), "https://gitlab.com/TheGreatCodFather/CS-Phone-App/blob/master/LICENSE");
            }
        });


        /**
         * A lovely little easter egg for my efforts
         */
        final TextView prusInator = (TextView) view.findViewById(R.id.prusInator);
        final ScrollView scrollView = (ScrollView) view.findViewById(R.id.aboutScrollView);

        prusInator.setOnClickListener(new View.OnClickListener() {
            int clickCount = 0;

            @Override
            public void onClick(View v) {
                ++clickCount;

                if (clickCount >= 7) {
                    try {
                        scrollView.setVisibility(View.GONE);

                        final VideoView prusInatorVideoView = (VideoView) getActivity().findViewById(R.id.prusInatorVideoView);
                        prusInatorVideoView.setVisibility(View.VISIBLE);
                        String path = "android.resource://" + getActivity().getPackageName() + "/" + R.raw.prusinator;
                        prusInatorVideoView.setVideoURI(Uri.parse(path));
                        prusInatorVideoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                            @Override
                            public void onCompletion(MediaPlayer mp) {
                                scrollView.setVisibility(View.VISIBLE);
                                prusInatorVideoView.setVisibility(View.GONE);
                            }
                        });
                        prusInatorVideoView.start();
                    } catch (Exception e) {
                        Log.e("", e.toString());
                    }
                }
            }
        });

        // Inflate the layout for this fragment
        return view;
    }
}
