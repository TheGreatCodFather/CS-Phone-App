package com.eggprophet.cs_app.ui_fragments;

import java.lang.ref.WeakReference;
import java.util.EnumMap;
import java.util.Map;

import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;

import com.eggprophet.cs_app.R;
import com.eggprophet.cs_app.powerschool_backend.PowerschoolFunctions;

import static android.content.Context.MODE_PRIVATE;

public class StudentIDFragment extends Fragment {
    private MenuItem mScanItem = null;
    private SharedPreferences settings = null;
    private View view = null;
    private PowerschoolFunctions pw;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        setRetainInstance(true);
        setHasOptionsMenu(true);

        view = inflater.inflate(R.layout.student_id_fragment, container, false);

        settings = getActivity().getSharedPreferences("CPP_APP_PREFs", MODE_PRIVATE);

        pw = new PowerschoolFunctions();


        ImageView studentPic = (ImageView) view.findViewById(R.id.student_id_image);
        byte[] decoded = Base64.decode(pw.imageBase64, Base64.DEFAULT);

        Bitmap bMap = BitmapFactory.decodeByteArray(decoded, 0, decoded.length);

        if (bMap != null) {
            studentPic.setImageBitmap(bMap);
        }

        return view;
    }



    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        mScanItem = menu.findItem(R.id.action_scan);
        mScanItem.setVisible(true);
    }

    @Override
    public void onDestroyView() {
        mScanItem.setVisible(false);
        super.onDestroyView();
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshBarcode(view, pw.returned.getReturn().getUserSessionVO().getStudentIDs().get(0).toString());
        for (Long element : pw.returned.getReturn().getUserSessionVO().getStudentIDs()) {
            System.out.println(element);
        }
    }

    public void refreshBarcode(View view, String barcodeData) {
        AsyncBarcodeGen runner = new AsyncBarcodeGen(view);
        runner.execute(barcodeData);

        TextView barcodeTxt = (TextView) view.findViewById(R.id.student_id_text);
        barcodeTxt.setText(barcodeData);
    }

    /* Our background task for this fragment */
    private class AsyncBarcodeGen extends AsyncTask<String, Bitmap, Bitmap> {
        private WeakReference vRef;

        public AsyncBarcodeGen(View v) {
            vRef = new WeakReference(v);
        }

        @Override
        protected Bitmap doInBackground(String... params) {
            Bitmap barcode_bitmap = null;

            try {
                barcode_bitmap = encodeAsBitmap(params[0], BarcodeFormat.CODE_39, 800, 150);
            } catch (WriterException e) {
                e.printStackTrace();
            }

            return barcode_bitmap;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            if (vRef != null) {
                final View view = (View) vRef.get();
                if (view != null) {
                    ImageView barcodeImg = (ImageView) view.findViewById(R.id.student_id_barcode);
                    barcodeImg.setImageBitmap(bitmap);
                    view.findViewById(R.id.loadingPanel).setVisibility(View.GONE);
                }
            }
        }


        /**************************************************************
         * Using Google's Zxing API for barcode generation.
         *
         * https://zxing.github.io/zxing/apidocs/
         */

        private static final int ALPHA = 0x00FFFFFF;
        private static final int BLACK = 0xFF000000;

        Bitmap encodeAsBitmap(String contents, BarcodeFormat format, int img_width, int img_height) throws WriterException {
            String contentsToEncode = contents;
            if (contentsToEncode == null) {
                return null;
            }
            Map<EncodeHintType, Object> hints = null;
            String encoding = guessAppropriateEncoding(contentsToEncode);
            if (encoding != null) {
                hints = new EnumMap<EncodeHintType, Object>(EncodeHintType.class);
                hints.put(EncodeHintType.CHARACTER_SET, encoding);
            }
            MultiFormatWriter writer = new MultiFormatWriter();
            BitMatrix result;
            try {
                result = writer.encode(contentsToEncode, format, img_width, img_height, hints);
            } catch (IllegalArgumentException iae) {
                // Unsupported format
                return null;
            }
            int width = result.getWidth();
            int height = result.getHeight();
            int[] pixels = new int[width * height];
            for (int y = 0; y < height; y++) {
                int offset = y * width;
                for (int x = 0; x < width; x++) {
                    pixels[offset + x] = result.get(x, y) ? BLACK : ALPHA;
                }
            }

            Bitmap bitmap = Bitmap.createBitmap(width, height,
                    Bitmap.Config.ARGB_8888);
            bitmap.setPixels(pixels, 0, width, 0, 0, width, height);
            return bitmap;
        }

        private String guessAppropriateEncoding(CharSequence contents) {
            // Very crude at the moment
            for (int i = 0; i < contents.length(); i++) {
                if (contents.charAt(i) > 0xFF) {
                    return "UTF-8";
                }
            }
            return null;
        }
    }
}
