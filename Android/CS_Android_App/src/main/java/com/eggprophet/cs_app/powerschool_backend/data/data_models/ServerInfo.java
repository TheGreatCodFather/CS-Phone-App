
package com.eggprophet.cs_app.powerschool_backend.data.data_models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ServerInfo {

    @SerializedName("@type")
    @Expose
    private String type;
    @SerializedName("apiVersion")
    @Expose
    private String apiVersion;
    @SerializedName("dayLightSavings")
    @Expose
    private Long dayLightSavings;
    @SerializedName("parentSAMLEndPoint")
    @Expose
    private String parentSAMLEndPoint;
    @SerializedName("publicPortalDisabled")
    @Expose
    private Boolean publicPortalDisabled;
    @SerializedName("publicPortalDisabledMessage")
    @Expose
    private PublicPortalDisabledMessage publicPortalDisabledMessage;
    @SerializedName("rawOffset")
    @Expose
    private Long rawOffset;
    @SerializedName("serverTime")
    @Expose
    private String serverTime;
    @SerializedName("studentSAMLEndPoint")
    @Expose
    private String studentSAMLEndPoint;
    @SerializedName("teacherSAMLEndPoint")
    @Expose
    private String teacherSAMLEndPoint;
    @SerializedName("timeZoneName")
    @Expose
    private String timeZoneName;

    /**
     * No args constructor for use in serialization
     * 
     */
    public ServerInfo() {
    }

    /**
     * 
     * @param apiVersion
     * @param dayLightSavings
     * @param teacherSAMLEndPoint
     * @param publicPortalDisabledMessage
     * @param publicPortalDisabled
     * @param serverTime
     * @param rawOffset
     * @param studentSAMLEndPoint
     * @param timeZoneName
     * @param type
     * @param parentSAMLEndPoint
     */
    public ServerInfo(String type, String apiVersion, Long dayLightSavings, String parentSAMLEndPoint, Boolean publicPortalDisabled, PublicPortalDisabledMessage publicPortalDisabledMessage, Long rawOffset, String serverTime, String studentSAMLEndPoint, String teacherSAMLEndPoint, String timeZoneName) {
        super();
        this.type = type;
        this.apiVersion = apiVersion;
        this.dayLightSavings = dayLightSavings;
        this.parentSAMLEndPoint = parentSAMLEndPoint;
        this.publicPortalDisabled = publicPortalDisabled;
        this.publicPortalDisabledMessage = publicPortalDisabledMessage;
        this.rawOffset = rawOffset;
        this.serverTime = serverTime;
        this.studentSAMLEndPoint = studentSAMLEndPoint;
        this.teacherSAMLEndPoint = teacherSAMLEndPoint;
        this.timeZoneName = timeZoneName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public ServerInfo withType(String type) {
        this.type = type;
        return this;
    }

    public String getApiVersion() {
        return apiVersion;
    }

    public void setApiVersion(String apiVersion) {
        this.apiVersion = apiVersion;
    }

    public ServerInfo withApiVersion(String apiVersion) {
        this.apiVersion = apiVersion;
        return this;
    }

    public Long getDayLightSavings() {
        return dayLightSavings;
    }

    public void setDayLightSavings(Long dayLightSavings) {
        this.dayLightSavings = dayLightSavings;
    }

    public ServerInfo withDayLightSavings(Long dayLightSavings) {
        this.dayLightSavings = dayLightSavings;
        return this;
    }

    public String getParentSAMLEndPoint() {
        return parentSAMLEndPoint;
    }

    public void setParentSAMLEndPoint(String parentSAMLEndPoint) {
        this.parentSAMLEndPoint = parentSAMLEndPoint;
    }

    public ServerInfo withParentSAMLEndPoint(String parentSAMLEndPoint) {
        this.parentSAMLEndPoint = parentSAMLEndPoint;
        return this;
    }

    public Boolean getPublicPortalDisabled() {
        return publicPortalDisabled;
    }

    public void setPublicPortalDisabled(Boolean publicPortalDisabled) {
        this.publicPortalDisabled = publicPortalDisabled;
    }

    public ServerInfo withPublicPortalDisabled(Boolean publicPortalDisabled) {
        this.publicPortalDisabled = publicPortalDisabled;
        return this;
    }

    public PublicPortalDisabledMessage getPublicPortalDisabledMessage() {
        return publicPortalDisabledMessage;
    }

    public void setPublicPortalDisabledMessage(PublicPortalDisabledMessage publicPortalDisabledMessage) {
        this.publicPortalDisabledMessage = publicPortalDisabledMessage;
    }

    public ServerInfo withPublicPortalDisabledMessage(PublicPortalDisabledMessage publicPortalDisabledMessage) {
        this.publicPortalDisabledMessage = publicPortalDisabledMessage;
        return this;
    }

    public Long getRawOffset() {
        return rawOffset;
    }

    public void setRawOffset(Long rawOffset) {
        this.rawOffset = rawOffset;
    }

    public ServerInfo withRawOffset(Long rawOffset) {
        this.rawOffset = rawOffset;
        return this;
    }

    public String getServerTime() {
        return serverTime;
    }

    public void setServerTime(String serverTime) {
        this.serverTime = serverTime;
    }

    public ServerInfo withServerTime(String serverTime) {
        this.serverTime = serverTime;
        return this;
    }

    public String getStudentSAMLEndPoint() {
        return studentSAMLEndPoint;
    }

    public void setStudentSAMLEndPoint(String studentSAMLEndPoint) {
        this.studentSAMLEndPoint = studentSAMLEndPoint;
    }

    public ServerInfo withStudentSAMLEndPoint(String studentSAMLEndPoint) {
        this.studentSAMLEndPoint = studentSAMLEndPoint;
        return this;
    }

    public String getTeacherSAMLEndPoint() {
        return teacherSAMLEndPoint;
    }

    public void setTeacherSAMLEndPoint(String teacherSAMLEndPoint) {
        this.teacherSAMLEndPoint = teacherSAMLEndPoint;
    }

    public ServerInfo withTeacherSAMLEndPoint(String teacherSAMLEndPoint) {
        this.teacherSAMLEndPoint = teacherSAMLEndPoint;
        return this;
    }

    public String getTimeZoneName() {
        return timeZoneName;
    }

    public void setTimeZoneName(String timeZoneName) {
        this.timeZoneName = timeZoneName;
    }

    public ServerInfo withTimeZoneName(String timeZoneName) {
        this.timeZoneName = timeZoneName;
        return this;
    }

}
