package com.eggprophet.cs_app;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;

/**
 * Created by eggprophet on 05/02/17.
 */

public class Util {
    public static void openWebPage(Activity act, String url) {
        Uri webpage = Uri.parse(url);
        Intent intent = new Intent(Intent.ACTION_VIEW, webpage);
        intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        if (intent.resolveActivity(act.getPackageManager()) != null) {
            act.startActivity(intent);
        }
    }
}
