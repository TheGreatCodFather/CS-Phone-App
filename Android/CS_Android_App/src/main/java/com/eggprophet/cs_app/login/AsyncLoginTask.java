package com.eggprophet.cs_app.login;

import android.os.AsyncTask;

import static com.eggprophet.cs_app.powerschool_backend.PowerschoolFunctions.pcasLogin;

public class AsyncLoginTask extends AsyncTask<String, Void, Boolean> {
    public static AsyncResponse delegate = null;
    private LoginCrypto crypt = new LoginCrypto();
    private String option = null;

    @Override
    protected Boolean doInBackground(String... params) {
        crypt.LoginCrypto();
        option = params[0];

        switch(option) {
            case "doLogin":
                if (doLogin(params[1], params[2], params[3])) { return true; }
                break;
            case "genKey":
                if (genKey(params[1])) { return true; }
                break;
        }
        return false;
    }

    @Override
    protected void onPostExecute(Boolean result) {
        switch(option) {
            case "doLogin":
                delegate.loginFinished(result);
                break;
            case "genKey":
                delegate.genKeyFinished(result);
                break;
        }
    }

    private boolean doLogin(String districtADDR, String usr, String passwd) {
        if (pcasLogin(districtADDR, usr, passwd)) {
            return true;
        }
        return false;
    }

    private boolean genKey(String alias) {
        return crypt.genKey(alias);
    }
}
